# 进度管理数据爬取
from utils import request_util
from utils import logging_method as log
from lxml import etree
from entity import progress_day_footage
from mysql.mysql_connector import MysqlConnector
import time
from entity import crawler_config

# 进度列表url("reportDate")
progress_list_url = "https://apps.r93535.com/sdxxh/dr/listJson?eq_rgdid=%s&pageSize=20000&page=1"
# 单个工点信息
construct_point_url = "https://apps.r93535.com/sdxxh/gd/gdview?rgdid=%s&rtid=71663"


def get_construct_points():
    """
    获取工点信息列表
    :return:
    """
    log.info("录入工点信息(progress_construct_point)")
    # 请求工点信息
    response = request_util.get_retry_std(crawler_config.get_conf("construct_points_url"))
    connector = MysqlConnector("progress_construct_point")
    for data in response["rows"]:
        construct_point_id = data["id"]
        construct_point = {
            "delete_flag": "0",
            "name": data["name"],
            "construct_status": data["statusFlag"],
            "start_mile": data["startMilestoneWithDK"],
            "end_mile": data["endMilestoneWithDK"],
            "distance": abs(float(data["startMilestoneWithFloat"]) - float(data["endMilestoneWithFloat"])),
            "plan_start_date": data["jhkgDate"],
            "plan_end_date": data["jhwcDate"],
            "start_date": data["sjkgDate"],
            "cal_end_date": data["tywcDate"],
            "progress_status": data["jdAlert"] - 1,
            "step_status": data["bjAlert"] - 1,
            "order_num": "0",
            "construct_point_id": construct_point_id,
            "modify_date": data["modifyDate"]
        }
        if "ygWyActAlarmMilesStr" in data:
            construct_point["inverted_arch"] = data["ygWyActAlarmMilesStr"]
        if "ecWyActAlarmMilesStr" in data:
            construct_point["secondary_lining"] = data["ecWyActAlarmMilesStr"]
        # 处理工点信息
        this_construct = exist_construct_point(construct_point_id)
        if this_construct is not None and this_construct["construct_point_id"] is not None:
            # 存在该工点，判断是否需要修改
            last_modify_date = this_construct["modify_date"]
            if last_modify_date != data["modifyDate"]:
                condition = "where construct_point_id = '%s'" % construct_point_id
                sql = connector.update(construct_point, condition)
                connector.operation(sql)
        else:
            # 不存在该工点，插入数据
            sql = connector.insert(construct_point)
            connector.operation(sql)
        # 每个建设中工点的进度、已完成统计
        if data["statusFlag"] == 1:
            log.info("录入工点已完成（昨日，周，月，已完成）进尺统计，工点名称：%s" % data["name"])
            get_progress_static(data)
        # 每个工点的日进尺信息
        if data["statusFlag"] == 1:
            log.info("录入工点进尺数据，工点名称：%s" % data["name"])
            get_progress_footage_list(data["rgdID"], construct_point_id)
    return response["rows"]


def get_progress_static(construct_info):
    """
    统计工点进度，日、周、月，已完成等
    :param construct_info:
    :return:
    """
    modify_date = construct_info["modifyDate"]
    construct_point_id = construct_info["id"]
    count_sql = "select count(1) from progress_construct_point_statistic where modify_date = '%s' and construct_point_id = '%s'" % (modify_date, construct_point_id)
    connector = MysqlConnector("progress_construct_point_statistic")
    # 避免数据重复
    if connector.operation(count_sql)[0][0] != 0:
        return
    now = get_now_date_time()
    progress_static = {
        "yesterday_progress": construct_info["daykwlength"],
        "week_progress": construct_info["weekkwlength"],
        "month_progress": construct_info["monthkwlength"],
        "complete_distance": construct_info["finishLengthWithFloat"],
        "construct_point_id": construct_point_id,
        "create_date": now,
        "delete_flag": 0,
        "modify_date": modify_date
    }
    sql = connector.insert(progress_static)
    connector.operation(sql)


def get_progress_footage_list(eq_rgdid, construct_point_id):
    """
    获取进尺进度列表
    :return:
    """
    # 请求工点步距/阈值
    step_threshold = get_construct_point_info(eq_rgdid)
    # 请求每日进尺列表
    response = request_util.get_retry_std(progress_list_url % eq_rgdid)
    last_footage_report = progress_day_footage.get_last_footage_report(construct_point_id)
    record_ids_and_modify_dates = progress_day_footage.get_record_id_and_modify_date(construct_point_id)
    log.info("工点id：%s, eq_rgdid： %s, 上次上报进尺：%s" % (construct_point_id, eq_rgdid, last_footage_report))
    connector = MysqlConnector("progress_day_footage")
    now = get_now_date_time()
    for data in response["rows"]:
        footage = {
            "delete_flag": "0",
            "report_date": data["reportDate"],
            "report_update_date": data["modifyTime"],
            "face_front_mileage": data["zzmMileStoneWithDK"],
            "face_footage": data["zzmFootageWithFloat"],
            "face_plan_footage": data["zzmJhFootageWithFloat"],
            "invert_front_mileage": data["ygMileStoneWithDK"],
            "invert_footage": data["ygFootageWithFloat"],
            "invert_step": step_threshold[0],
            "invert_threshold": step_threshold[1],
            "secondary_lining_front_mileage": data["ecMileStoneWithDK"],
            "secondary_lining_footage": data["ecFootageWithFloat"],
            "secondary_lining_step": step_threshold[2],
            "secondary_lining_threshold": step_threshold[3],
            "surrounding_rock_plan_type": data["wyDesignStr"],
            "surrounding_rock_actual_type": data["wyActStr"],
            "reporter": data["reportUser"],
            "construct_point_id": construct_point_id,
            "record_id": data["id"],
            "create_date": now
        }
        # 排除重复数据
        record_id_and_modify_date = str(data["id"]) + "_" + data["modifyTime"]
        if record_id_and_modify_date in record_ids_and_modify_dates:
            continue
        last_id = last_footage_report["record_id"]
        # 修改数据
        if int(last_id) >= data["id"]:
            # 存在记录且修改过，则更新数据
            footage["update_date"] = now
            condition = "where record_id = '%s' and report_update_date != '%s'" % (data["id"], data["modifyTime"])
            sql = connector.update(footage, condition)
            connector.operation(sql)
        # 插入数据
        else:
            # 不存在记录，直接插入
            footage["create_date"] = now
            sql = connector.insert(footage)
            connector.operation(sql)


def day_footage_empty():
    """
    日进尺记录是否为空
    :return:
    """
    sql = "select count(1) from progress_day_footage where delete_flag = 0"
    connector = MysqlConnector("progress_day_footage")
    raw_data = connector.operation(sql)
    return raw_data[0][0] == 0


def get_construct_point_info(rgdid=208178):
    """
    获取单个工点的步距/阈值
    :return: [仰拱步距，仰拱阈值，二衬步距，二衬阈值]
    """
    response = request_util.get_retry(construct_point_url % rgdid)
    tree = etree.HTML(response.text)
    if len(tree.xpath("//td[@id='ygbjAlert']")) == 0:
        return [0, 0, 0, 0]
    text = tree.xpath("//td[@id='ygbjAlert']")[0].text
    data1 = text.replace("\r\n\t\t\t\t", "").replace(" ", "").replace("\xa0", "").split("/")
    data2 = tree.xpath("//td[@id='ecbjAlert']")[0].text.replace("\xa0", "").split("/")
    return data1 + data2


def exist_construct_point(construct_point_id):
    """
    用于判断是否存在该工点
    :param construct_point_id:
    :return:
    """
    sql = "select id,simple_name,modify_date from progress_construct_point where delete_flag = 0 and construct_point_id = '%s'" % construct_point_id
    connector = MysqlConnector("progress_construct_point")
    raw_data = connector.operation(sql)
    if raw_data is None or len(raw_data) == 0:
        return None
    return {"construct_point_id": raw_data[0][0], "simple_name": raw_data[0][1], "modify_date": str(raw_data[0][2])}


def get_now_date_time():
    return time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())