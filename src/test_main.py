import unittest
from utils import captcha_util


class TestCaptcha(unittest.TestCase):

    def test_analyse_accuracy(self):
        """
        测试识别精确度,入参为循环次数
        :return:
        """
        captcha_util.test_accuracy(20)

    def test_bound_result(self):
        """
        测试边界划分
        :return:
        """
        captcha_util.test_bound_result(100)

    def test_mark_images(self):
        """
        人工标注切图并保存
        :return:
        """
        for i in range(2):
            captcha_util.cut_and_mark()


if __name__ == '__main__':
    unittest.main()
