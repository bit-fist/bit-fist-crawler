from utils import config_util

section = "mysql"


def host():
    return config_util.get(section, "host")


def port():
    return config_util.get_int(section, "port")


def user():
    return config_util.get(section, "user")


def password():
    return config_util.get(section, "password")


def database():
    return config_util.get(section, "database")


def charset():
    return config_util.get(section, "charset")