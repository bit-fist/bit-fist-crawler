from utils import config_util

section = "crawler"


def get_conf(option):
    return config_util.get(section, option)
