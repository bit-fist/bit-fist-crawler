"""# -*- coding: utf-8 -*-"""
import unittest
import login
import progress_collector
import time


class TestLogin(unittest.TestCase):
    def test_progress(self):
        """
        测试进度数据
        :return:
        """
        login.login()
        progress_collector.get_construct_points()
        # progress_collector.get_progress_list()
        # progress_collector.get_construct_point_info()

    def test_captcha(self):
        print(time.strftime("%Y-%m-%d %H:%M:%S", time.localtime()))


if __name__ == '__main__':
    unittest.main()
