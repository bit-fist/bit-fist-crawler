FROM python:3.8.10-slim-buster

ENV WORK_PATH /home/project

EXPOSE 11423

WORKDIR $WORK_PATH

COPY ./src $WORK_PATH/
COPY requirements.txt $WORK_PATH/

RUN python -m pip install -r $WORK_PATH/requirements.txt -i https://pypi.douban.com/simple

VOLUME ["/home/project/config", "/home/project/logs"]

ENTRYPOINT ["python"]

CMD ["starter.py"]