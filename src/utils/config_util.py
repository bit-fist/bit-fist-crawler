import configparser
import os

config = configparser.ConfigParser()
root_path = os.path.split(os.path.dirname(os.path.abspath(__file__)))[0]
config.read(root_path + "/config/config.txt", encoding="utf-8")


def get(section, option):
    return config.get(section, option)


def get_int(section, option):
    return config.getint(section, option)


def get_bool(section, option):
    return config.getboolean(section, option)


def get_float(section, option):
    return config.getfloat(section, option)
