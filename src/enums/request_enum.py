from enum import Enum

request_method = Enum("request_method", ("GET", "POST", "PUT", "DELETE"))
