#!/usr/bin/python3
from crawler import progress_collector
from crawler import login
from apscheduler.schedulers.blocking import BlockingScheduler
import logging
import sys
import os
sys.path.append(os.path.dirname(os.path.abspath(__file__)))


def main():
    # 登录，收集数据
    login.login()
    data_collector()
    # 定时收集数据
    task = BlockingScheduler()
    task.add_job(data_collector, "cron", hour="7-23", minute="*/30")
    task.start()


def data_collector():
    """
    数据收集
    :return:
    """
    logging.info("开始定时任务")
    # 同步进度信息
    progress_collector.get_construct_points()
    logging.info("定时任务完成")


if __name__ == '__main__':
    main()

