# 每日进尺信息
from mysql.mysql_connector import MysqlConnector

table_name = "progress_day_footage"


class ProgressDayFootage:
    def insert(self, data):
        pass


def get_last_footage_report(construct_point_id):
    sql = "select record_id,report_update_date from progress_day_footage where delete_flag = 0 and construct_point_id='%s' order by report_date desc limit 1" % construct_point_id
    connector = MysqlConnector(table_name)
    last_report = connector.operation(sql)
    if last_report is None or len(last_report) == 0 or len(last_report[0]) == 0:
        return {"record_id": "0", "report_update_date": "2020-01-01 00:00:00"}
    return {"record_id": last_report[0][0], "report_update_date": str(last_report[0][1])}


def get_record_id_and_modify_date(construct_point_id):
    sql = "select concat(record_id, '_', report_update_date) from progress_day_footage where delete_flag = 0 and construct_point_id='%s' order by report_date desc" % construct_point_id
    connector = MysqlConnector(table_name)
    raw_data = connector.operation(sql)
    result = []
    for data in raw_data:
        result.append(data[0])
    return result

